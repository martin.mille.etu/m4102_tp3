## Développement d'une ressource *commande*

### API et représentation des données

| URI            	   | Opération   | MIME 										 | Requête 		     | Réponse                                                            |
| :--------------	   | :---------- | :---------------------------------------------| :--               | :----------------------------------------------------------------- | 
| /commandes      	   | GET         | <-application/json<br><-application/xml       |                   | liste des commandes et leurs liste de pizzas                       |
| /commandes/{id} 	   | GET         | <-application/json<br><-application/xml       |                   | une commande ou 404                                                |
| /commandes{id}/nom   | GET         | <-text/plain                                  |                   | le nom de l'acheteur ou 404                                        | 
| /commandes            | POST        | <-/->application/json                         | Commande(C2)      | Nouvelle commandes(C2)<br>409 si la commande existe déjà (même nom) |
| /commandes/{id}       | DELETE      |                                               |                   |                                                                    |




Une commande comporte une liste des pizzas, un identifiant, un nom et un prénom. Sa
représentation JSON (C2) prendra donc la forme suivante :

    {
    [{
    [ {"id":"f38806a8-7c85-49ef-980c-149dcd81d200", "nom":"Chorizo"}
      { "id" :f38806a8-7c85-49ef-980c-149dcd81d404 ", "nom" : "Reine" ],
		"id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "nom": "Toto",
	  "prénom":"Titi"
    }


