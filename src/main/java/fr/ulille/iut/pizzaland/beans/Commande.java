package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.CommandeDto;

public class Commande {
	private String nom;
	private String prenom;
	private ArrayList<Pizza> pizzaList;
	private UUID id = UUID.randomUUID();
	
	public Commande() {
		
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public ArrayList<Pizza> getPizzaList() {
		return pizzaList;
	}

	public void setPizzaList(ArrayList<Pizza> pizzaList) {
		this.pizzaList = pizzaList;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Commande(String nom, String prenom, ArrayList<Pizza> pizzaList) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.pizzaList = pizzaList;
	}
	
	public static CommandeDto toDto(Commande commande) {
		CommandeDto dto = new CommandeDto();
		dto.setId(commande.getId());
		dto.setNom(commande.getNom());
		dto.setPrenom(commande.getPrenom());
		dto.setPizzaList(commande.getPizzaList());
		return dto;
	}
	
}
