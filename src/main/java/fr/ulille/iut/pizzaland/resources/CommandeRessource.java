package fr.ulille.iut.pizzaland.resources;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDAO;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;

@Path("/commandes")
@Produces("application/json")
public class CommandeRessource {
	private static final Logger LOGGER = Logger.getLogger(CommandeRessource.class.getName());

	private CommandeDao commandes;

	@Context
	public UriInfo uriInfo;

	public CommandeRessource() {
		commandes = BDDFactory.buildDao(CommandeDao.class);
		commandes.createTableAndPizzaAssociation();
	}
	
	@GET
    public List<CommandeDto> getAll() {
        LOGGER.info("CommandeRessource:getAll");
        List<CommandeDto> listeCommande = commandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
        LOGGER.info(listeCommande.toString());
        return listeCommande;
    }

}
