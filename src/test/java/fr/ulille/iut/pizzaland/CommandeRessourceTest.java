package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.Response;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDAO;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.Response;

public class CommandeRessourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(CommandeRessourceTest.class.getName());
	private IngredientDAO ingredients;
	private PizzaDao pizzas;
	private CommandeDao commandes ;
	

	@Before
	public void setEnvUp() {
		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createTableAndIngredientAssociation();;
		ingredients = BDDFactory.buildDao(IngredientDAO.class);
		ingredients.createTable();
		commandes = BDDFactory.buildDao(CommandeDao.class);
		commandes.createTableAndPizzaAssociation();
	}

	@After
	public void tearEnvDown() throws Exception {
		pizzas.dropTable();
		ingredients.dropTable();
		commandes.dropTable();
	}
	
	
	@Test
	public void testGetEmptyList() {
	   Response response = target("/commandes").request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		List<CommandeDto> commandes;
		commandes = response.readEntity(new GenericType<List<CommandeDto>>() {
		});
		assertEquals(0, commandes.size());
	}
	
}
