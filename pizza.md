 URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /pizzas            | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas(I2)                                           |
| /pizzas/{id}        | GET         | <-application/json<br><-application/xml                      |                 | une pizza(I2) ou 404                                            |
| /pizzas/{id}/ingredients| GET     | <-application/json<br><-application/xml                     |                 | liste des ingrédients de la pizza
	/pizzas/{id}/name   | GET         | <-text/plain                                                 |                 | le nom de la pizza ou 404                                        |
| /pizzas            | POST        | <-/->application/json<br>->application/x-www-form-urlencoded |              | Nouvelle pizza (I2)<br>409 si la pizza existe déjà (même nom) |
| /pizzas/{id}       | DELETE      | <-application/json<br><-application/xml                      |                 | supprime la pizza 
| /pizzas/ingredientDelete/{id}/{ingredient} | UPDATE | <-application/json<br><-application/xml                    |                 | supprime l'ingrédient de la pizza      

               
Une pizza sera représentée sous la forme : 
{
  "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
  "name": "reine"
  "ingrédients : { {tomate},{champignon},{jambon}
}

